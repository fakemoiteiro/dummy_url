# frozen_string_literal: true

require 'sinatra'
require 'jaeger/client'
require 'rack/tracer'

ENV['SERVER_NAME'] = 'dummy_url'
ENV['RACK_ENV'] = 'development'

OpenTracing.global_tracer = Jaeger::Client.build(
  host: 'jaeger',
  port: 6831,
  service_name: 'articles-dummy-url'
)

# DummyURL is a dummy service for a dummy purpose
class DummyURL < Sinatra::Base
  get '/' do
    'hello world'
  end
end

use Rack::Tracer

run Rack::Cascade.new([DummyURL])
