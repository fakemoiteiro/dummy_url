FROM ruby:2.7.1

ENV ENV production

WORKDIR /app

# installing dependencies before copy the entire application to create cache
COPY Gemfile Gemfile.lock /app/
RUN bundle install

# copy the application
COPY . /app

CMD ["bundle", "exec", "rackup", "-o", "0.0.0.0", "-p", "4001"]
